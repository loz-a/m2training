<?php

namespace Training\Orm\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class CustomerPriority extends AbstractSource
{
    public function getAllOptions(): array
    {
        $options = array_map(function($priority) {
            return [
                'label' => sprintf('Priority %d', $priority),
                'value' => $priority
            ];
        }, range(1, 10));
        
        $frontendInput = $this->getAttribute()->getFrontendInput();
        if ($frontendInput === 'select') {
            array_unshift($options, ['label' => '',  'value' => 0]);
        }
        
        return $options;
    }

}
