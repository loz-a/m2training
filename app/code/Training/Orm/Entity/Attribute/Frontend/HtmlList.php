<?php

namespace Training\Orm\Entity\Attribute\Frontend;

use Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend;
use Magento\Framework\DataObject;

class HtmlList extends AbstractFrontend
{
    const FIELD_MULTISELECT = 'multiselect';
    
    /**     
     * @param DataObject $object
     * @return string
     */
    public function getValue(DataObject $object)
    {
        if ($this->getConfigField('input') !== self::FIELD_MULTISELECT) {
            return parent::getValue($object);            
        }
        
        return $this->getValueAsHtml($object);
    }
    
    
    /**     
     * @param DataObject $object
     * @return string
     */
    protected function getValueAsHtml(DataObject $object)
    {
        $options = $this->getOptions($object);
        $escapedOptions = array_map('htmlspecialchars', $options);
        return sprintf('<ul><li>%s</li></ul>', implode('</li><li>', $escapedOptions));
    }
    
    
    /**     
     * @param DataObject $object
     * @return array
     */
    protected function getOptions(DataObject $object)
    {
        $optionId = $object->getData($this->getAttribute()->getAttributeCode());
        $option = $this->getOption($optionId);
        return is_array($option) ? $option : [$option];
    }
                
}
