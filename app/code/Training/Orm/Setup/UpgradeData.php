<?php

namespace Training\Orm\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\{
    Backend\ArrayBackend,
    ScopedAttributeInterface,
    Source\Table as TableSourceModel    
};
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\{ModuleContextInterface, ModuleDataSetupInterface, UpgradeDataInterface};
use Magento\Catalog\Setup\CategorySetupFactory;
use Training\Orm\Entity\Attribute\Frontend\HtmlList;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Training\Orm\Entity\Attribute\Source\CustomerPriority as CustomerPriorityAttrSource;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetup
     */
    private $eavSetup;
    
    /**
     *
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;
    
    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    public function __construct(
        EavSetup $eavSetup, 
        CategorySetupFactory $categorySetupFactory,
        CustomerSetupFactory $customerSetupFactory
    ){
        $this->eavSetup = $eavSetup;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {   
        $dbVersion = $context->getVersion();
        
        if (version_compare($dbVersion, '0.1.0', '<')) {

            $this->eavSetup->addAttribute(
                Product::ENTITY, 
                'compatible_condiments', 
                [
                    'label'            => 'Compatible Condiments',
                    'type'             => 'varchar',
                    'input'            => 'multiselect',
                    'source'           => TableSourceModel::class,
                    'backend'          => ArrayBackend::class,
                    'required'         => false,
                    'user_defined'     => true,
                    'global'           => ScopedAttributeInterface::SCOPE_WEBSITE,
                    'visible_on_front' => true,
                    'group'            => 'Product Details',
                    'option' => [
                        'values' => [
                            100 => 'Creme',
                            200 => 'Milk',
                            300 => 'Onions',
                            400 => 'Mustard',
                            500 => 'Ketchup',
                            600 => 'Sweet Chutney',
                            700 => 'Sugar',
                            800 => 'Honey',
                            900 => 'Chilly',
                        ]
                    ],
                ]);            
        }
        
        
        if (version_compare($dbVersion, '0.1.2', '<')) {
            $this
                ->categorySetupFactory->create(['setup' => $setup])
                ->updateAttribute(
                    Product::ENTITY,
                    'compatible_condiments',
                    [
                        'frontend_model' => HtmlList::class,
                        'is_html_allowed_on_front' => true
                    ]
                );
        }
        
        
        if (version_compare($dbVersion, '0.1.3', '<')) {
            $this
                ->customerSetupFactory->create(['setup' => $setup])
                ->addAttribute(
                    Customer::ENTITY,
                    'priority',
                    [
                        'label' => 'Priority',
                        'type' => 'int',
                        'input' => 'select',
                        'source' => CustomerPriorityAttrSource::class,
                        'required' => 0,
                        'system' => 0,
                        'position' => 100
                    ]
                )
                ->getEavConfig()
                ->getAttribute('customer', 'priority')
                ->setData('used_in_forms', ['adminhtml_customer'])
                ->save();
        }
        
    }
}