<?php

namespace Training\Orm\Setup;

use Magento\Catalog\Model\{
    Product,
    ResourceModel\Eav\Attribute as CatalogAttribute
};
use Magento\Catalog\Setup\{CategorySetup, CategorySetupFactory};
use Magento\Framework\Setup\{InstallDataInterface, ModuleContextInterface, ModuleDataSetupInterface};


class InstallData implements InstallDataInterface
{
    private $categorySetupFactory;
    
    public function __construct(CategorySetupFactory $categorySetupFactory)
    {
        $this->categorySetupFactory = $categorySetupFactory;
    }
    
    public function install(
        ModuleDataSetupInterface $setup, 
        ModuleContextInterface $context
    ){
        $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
        $categorySetup->addAttribute(
            Product::ENTITY,
            'flavor_from_setup_method',
            [
                'label' => 'Flavor From Setup Method',
                'visible_on_front' => true,
                'required' => false,
                'global' => CatalogAttribute::SCOPE_STORE
            ]
        );
    }
    
}
