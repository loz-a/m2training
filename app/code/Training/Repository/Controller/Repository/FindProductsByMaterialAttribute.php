<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Eav\Model\Config as EavConfig;

class FindProductsByMaterialAttribute extends Action
{
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var EavConfig
     */
    protected $eavConfig;
    
    public function __construct(
        Context $context,
        ProductCollectionFactory $productCollectionFactory,
        EavConfig $eavConfig
    ){
        $this->productCollectionFactory = $productCollectionFactory;
        $this->eavConfig = $eavConfig;
        
        parent::__construct($context);
    }

    public function execute()
    {
        $materialAttribute = $this->eavConfig->getAttribute('catalog_product', 'material');        
        $optionId = $materialAttribute->getSource()->getOptionId('Canvas');
        
        $productCollection = $this->productCollectionFactory->create();
        $productCollection->addAttributeToFilter('material', ['finset' => $optionId]);
        
        foreach ($productCollection as $product) {
            var_dump($product->getId());
        }
        die;
    }

}
