<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Framework\Api\{FilterBuilder, SearchCriteriaBuilder, Search\FilterGroupBuilder, SortOrderBuilder};
use Magento\Customer\Api\{CustomerRepositoryInterface, Data\CustomerInterface};


class Customer extends Action
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepo;
    
    /**
     * @var SearchCriteriaBuilder
     */
    protected  $searchCriteriaBuilder;
    
     /**
     * @var FilterBuilder
     */
    protected $filterBuilder;
        
    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;
    
    public function __construct(
        Context $context, 
        CustomerRepositoryInterface $customerRepo,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder
    ){
        $this->customerRepo = $customerRepo;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        
        parent::__construct($context);
    }
    
    
    public function execute()
    {
        $customers = $this->getCustomersFromRepository();
        $this->outputResult($customers);
    }
    
    
    protected function getCustomersFromRepository()
    {
        $searchCriteria = $this->createSearchCriteria();
        $customers = $this->customerRepo->getList($searchCriteria);
        return $customers->getItems();
    }
    
    
    protected function createSearchCriteria()
    {
        $filterGroup = $this
            ->filterGroupBuilder // Logic OR
            ->addFilter($this->createEmailFilter())
            ->addFilter($this->createNameFilter())
            ->create();
        
        return $this
            ->searchCriteriaBuilder
            ->setFilterGroups([$filterGroup])
            ->create();
    }
    
    
    protected function createEmailFilter()
    {
        return $this
            ->filterBuilder
            ->setField('email')
            ->setValue('%@domain.com')
            ->setConditionType('like')
            ->create();
    }
    
    
    protected function createNameFilter()
    {
        return $this
            ->filterBuilder
            ->setField('firstname')
            ->setValue('Veronica')
            ->setConditionType('eq')
            ->create();
    }
    

    protected function customerToStr(CustomerInterface $customer)
    {
        return sprintf(
            '"%s %s" <%s> (%s)<br/>',
            $customer->getFirstname(),
            $customer->getLastname(),
            $customer->getEmail(),
            $customer->getId()
        );
    }
    
    
    protected function outputResult($customers)
    {
        $outputs = [];
        foreach ($customers as $customer) {
            $outputs[] = $this->customerToStr($customer);
        }
                
        if (count($customers)) {
            $this
                ->getResponse()
                ->appendBody(
                    sprintf('List contains %s <br/><br/>', get_class($customers[0]))
                );
        }
        
        $this
            ->getResponse()
            ->setHeader('Content-Type', 'text/html')            
            ->appendBody(sprintf('<html><body>%s</body></html>', implode('<br/>', $outputs)));
    }
}
