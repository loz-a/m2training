<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Framework\Api\{FilterBuilder, SearchCriteriaBuilder, Search\FilterGroupBuilder};
use Training\Repository\Api\ExampleRepositoryInterface;


class Example extends Action
{
    /**
     * @var ExampleRepositoryInterface
     */
    protected $exampleRepo;
    
    /**
     * @var SearchCriteriaBuilder
     */
    protected  $searchCriteriaBuilder;
    
     /**
     * @var FilterBuilder
     */
    protected $filterBuilder;
        
    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    
    public function __construct(
        Context $context, 
        ExampleRepositoryInterface $exampleRepo,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder
    ){
        $this->exampleRepo = $exampleRepo;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        
        parent::__construct($context);
    }
    
    
    public function execute()
    {        
        $filters = array_map(function($name) {
            return $this
                ->filterBuilder
                ->setConditionType('eq')
                ->setField('name')
                ->setValue($name)
                ->create();
            
        }, ['Foo', 'Bar', 'Baz', 'Qux']);
        
        $filtersGroup = $this
            ->filterGroupBuilder
            ->setFilters($filters)
            ->create();
        
        $searchCriteria = $this->createSearchCriteria($filtersGroup);
        $examples = $this->exampleRepo->getList($searchCriteria)->getItems();
        
        $this->outputResult($examples);
    }
        
    
    protected function createSearchCriteria($filtersGroup)
    {
        return $this
            ->searchCriteriaBuilder
            ->setFilterGroups([$filtersGroup])
            ->create();
    }


    
    protected function outputResult($examples)
    {
        $outputs = [];
        foreach ($examples as $example) {
            $outputs[] = sprintf('%s - (%d)', $example->getName(), $example->getId());
        }
                
        $this
            ->getResponse()
            ->setHeader('Content-Type', 'text/html')            
            ->appendBody(sprintf('<html><body>%s</body></html>', implode('<br/>', $outputs)));
    }
}
