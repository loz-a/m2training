<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Catalog\Api\{Data\ProductInterface, ProductRepositoryInterface};
use Magento\Framework\Api\{FilterBuilder, SearchCriteriaBuilder, Search\FilterGroupBuilder, SortOrderBuilder};
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableType;

class Product extends Action
{
    const PAGE_SIZE = 6;
    
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    
    /**
     * @var SearchCriteriaBuilder
     */
    protected  $searchCriteriaBuilder;
    
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;
        
    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;
    
    /**
     * @var SortOrderBuilder
     */
    protected $sortOrderBuilder;

    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepo,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->productRepository = $productRepo;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        
        parent::__construct($context);
    }
    
    
    public function execute()
    {
        $products = $this->getProductsFromRepository();
        
        $this->outputResult($products);
    }
    
    
    protected function getProductsFromRepository()
    {
        $searchCriteria = $this->createSearchCriteria();
        $products = $this->productRepository->getList($searchCriteria);
        return $products->getItems();
    }


    protected function createSearchCriteria()
    { 
        return $this
            ->searchCriteriaBuilder            
            ->setFilterGroups([ // Logic AND
                $this->getProductTypeFilter(),
                $this->getProductNameFilter()
            ])
            ->addSortOrder($this->getProductOrder())
            ->setPageSize(self::PAGE_SIZE)
            ->setCurrentPage(1)
            ->create();
    }
    
    
    protected function getProductOrder()
    {
        return $this
            ->sortOrderBuilder
            ->setField('entity_id')
            ->setAscendingDirection()
            ->create();
    }
    
    
    protected function getProductTypeFilter()
    {
        $filter = $this
            ->filterBuilder
            ->setField('type_id')
            ->setValue(ConfigurableType::TYPE_CODE)
            ->setConditionType('eq')
            ->create();
        
        return $this->filterGroupBuilder->addFilter($filter)->create();
    }
    
    
    protected function getProductNameFilter()
    {
        $filter = $this
            ->filterBuilder
            ->setField('name')
            ->setValue('M%')
            ->setConditionType('like')
            ->create();
        
        return $this->filterGroupBuilder->addFilter($filter)->create();
    }


    protected function productToStr(ProductInterface $product)
    {        
        return sprintf('%s - %s (%d)', $product->getName(), $product->getSku(), $product->getId());
    }
    
    
    protected function outputResult($products)
    {
        $outputs = [];
        foreach ($products as $product) {
            $outputs[] = $this->productToStr($product);
        }
        
        $result = sprintf('<html><body>%s</body></html>', implode('<br/>', $outputs));
        
        $this
            ->getResponse()
            ->setHeader('Content-Type', 'text/html')
            ->appendBody($result);
    }
}
