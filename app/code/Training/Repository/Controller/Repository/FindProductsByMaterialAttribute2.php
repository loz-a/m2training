<?php

namespace Training\Repository\Controller\Repository;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\Api\{FilterBuilder, Search\FilterGroupBuilder, SearchCriteriaBuilder};
use Magento\Catalog\Api\ProductRepositoryInterface;

class FindProductsByMaterialAttribute2 extends Action
{
    
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    
    /**
     * @var SearchCriteriaBuilder
     */
    protected  $searchCriteriaBuilder;
    
    /**
     * @var FilterBuilder
     */
    protected $filterBuilder;
    
    /**
     * @var FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var EavConfig
     */
    protected $eavConfig;
    
    public function __construct(
        Context $context,        
        EavConfig $eavConfig,
        ProductRepositoryInterface $productRepo,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder
    ){        
        $this->eavConfig = $eavConfig;
        $this->productRepository = $productRepo;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        
        parent::__construct($context);
    }

    public function execute()
    {
        $materialAttribute = $this->eavConfig->getAttribute('catalog_product', 'material');        
        $optionId = $materialAttribute->getSource()->getOptionId('Canvas');
        
        $filter = $this
            ->filterBuilder
            ->setField('material')
            ->setValue($optionId)
            ->setConditionType('finset')
            ->create();
        
        $filterGroup = $this
            ->filterGroupBuilder
            ->addFilter($filter)
            ->create();
        
        $searchCriteria = $this
            ->searchCriteriaBuilder
            ->setFilterGroups([$filterGroup])
            ->create();
        
        $products = $this->productRepository->getList($searchCriteria);
                
        foreach ($products->getItems() as $product) {
            var_dump($product->getId());
        }
        die;
    }

}
