<?php

namespace Training\Repository\Api\Data;


interface ExampleInterface
{
    public function setId($id);
    
    public function getId();
    
    public function setName($name);
    
    public function getName();
    
    public function setCreateAt($createAt);
    
    public function getCreateAt();
    
    public function setModifiedAt($modifiedAt);

    public function getModifiedAt();    
}
