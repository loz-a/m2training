<?php

namespace Training\Repository\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ExampleSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @api
     * @return ExampleInterface[]
     */
    public function getList();
    
    /**
     * @api
     * @param ExampleInterface[]
     * @return $this
     */
    public function setItems(array $items = null);
}
