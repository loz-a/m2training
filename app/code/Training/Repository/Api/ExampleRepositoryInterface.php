<?php

namespace Training\Repository\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ExampleRepositoryInterface
{
    public function getList(SearchCriteriaInterface $searchCriteria);
}
