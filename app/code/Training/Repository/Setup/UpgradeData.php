<?php

namespace Training\Repository\Setup;

use Magento\Framework\Setup\{ModuleContextInterface, ModuleDataSetupInterface, UpgradeDataInterface};

class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(
        ModuleDataSetupInterface $setup, 
        ModuleContextInterface $context
    ){
        $dbVersion = $context->getVersion();
        
        if (version_compare($dbVersion, '0.0.1', '<')) {
            $tableName = $setup->getTable('training_repository_example');
            
            $setup->getConnection()->insertMultiple($tableName, [
                ['example_id' => 1, 'name' => 'Foo'],
                ['example_id' => 2, 'name' => 'Bar'],
                ['example_id' => 3, 'name' => 'Baz'],
                ['example_id' => 4, 'name' => 'Qux'],
            ]);
        }
    }
}
