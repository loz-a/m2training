<?php

namespace Training\Repository\Model\Data;

use Training\Repository\Api\Data\ExampleInterface;

class Example implements ExampleInterface
{
    protected $id;
    protected $name;
    protected $createAt;
    protected $modifiedAt;


    public function getCreateAt()
    {
        return $this->createAt;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;
        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    
    public function __toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'createAt' => $this->createAt,
            'modifiedAt' => $this->modifiedAt
        ];
    }

}
