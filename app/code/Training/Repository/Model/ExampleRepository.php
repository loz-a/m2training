<?php

namespace Training\Repository\Model;

use Magento\Framework\Api\{Search\FilterGroup, SearchCriteriaInterface};
use Training\Repository\Api\{
    ExampleRepositoryInterface,
    ExampleSearchResultsInterfaceFactory
};
use Training\Repository\Model\ResourceModel\Example\Collection as ExampleCollection;

class ExampleRepository implements ExampleRepositoryInterface
{
    /**
     * @var ExampleSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;
    
    /**
     * @var Example
     */
    protected $exampleFactory;
        
    public function __construct(
        ExampleSearchResultsInterfaceFactory $searchResultsFactory,
        ExampleFactory $exampleFactory    
    ){
        $this->searchResultsFactory = $searchResultsFactory;
        $this->exampleFactory = $exampleFactory;        
    }
    
    
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->exampleFactory->create()->getCollection();
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        
        $this->applySearchCriteriaToCollection($searchCriteria, $collection);
        $examples = $this->convertCollectionToDataItemsArray($collection);
        
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($examples);
        
        return $searchResults;
    }
        
    
    protected function convertCollectionToDataItemsArray(
        ExampleCollection $collection
    ){
        return array_map(
            function(ExmpleDataModelRetrieveInterface $example) {
                return $example->getDataModel();                
            }, 
            $collection->getItems()
        );
    }

    
    protected function applySearchCriteriaToCollection(
        SearchCriteriaInterface $searchCriteria,
        ExampleCollection $collection
    ){
        $this->applySearchCriteriaFiltersToCollection($searchCriteria, $collection);
        $this->applySearchCriteriaSortOrdersToCollection($searchCriteria, $collection);
        $this->applySearchCriteriaPagingToCollection($searchCriteria, $collection);
    }
    
    
    protected function applySearchCriteriaFiltersToCollection(
        SearchCriteriaInterface $searchCriteria,
        ExampleCollection $collection
    ){
        foreach($searchCriteria->getFilterGroups() as $group) {            
            $this->addFilterGroupToCollection($group, $collection);
        }
    }
    
    
    protected function addFilterGroupToCollection(
        FilterGroup $filterGroup,
        ExampleCollection $collection
    ){
        $fields = [];
        $conditions = [];
        
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ?: 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    } 
    
    
    protected function applySearchCriteriaSortOrdersToCollection(
        SearchCriteriaInterface $searchCriteria,
        ExampleCollection $collection
    ){
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            foreach ($sortOrders as $sortOrder) {                
                $collection->addOrder($sortOrder->getField(), $sortOrder->getDirection());
            }
        }
    }
        
    
    protected function applySearchCriteriaPagingToCollection(
        SearchCriteriaInterface $searchCriteria,
        ExampleCollection $collection
    ){
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
    }
}
