<?php

namespace Training\Repository\Model;

use Magento\Framework\Model\AbstractModel;
use Training\Repository\Api\Data\{
    ExampleInterface,
    ExampleInterfaceFactory as ExampleDataFactory    
};

class Example extends AbstractModel
    implements ExampleInterface, ExmpleDataModelRetrieveInterface
{
    /**
     * @var ExampleDataFactory
     */
    protected $exampleDataFactory;
    
    public function __construct(
        \Magento\Framework\Model\Context $context, 
        \Magento\Framework\Registry $registry, 
        ExampleDataFactory $exampleDataFactory, 
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, 
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, 
        array $data = array()
    ) {        
        $this->exampleDataFactory = $exampleDataFactory;
        
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }
    
    
    protected function _construct()
    {
        $this->_init(ResourceModel\Example::class);
    }

    
    public function setName($name)
    {
        $this->setData('name', $name);
        return $this;
    }
    
    public function getName()
    {
        return $this->_getData('name');
    }
    
    public function setCreateAt($createAt)
    {
        $this->setData('create_at', $createAt);
        return $this;
    }
    
    public function getCreateAt()
    {
        return $this->_getData('create_at');
    }
    
    public function setModifiedAt($modifiedAt)
    {
        $this->setData('update_at', $modifiedAt);
        return $this;
    }

    public function getModifiedAt()
    {
        return $this->_getData('update_at');
    }
    
    
    public function getDataModel()
    {
        return $this
            ->exampleDataFactory
            ->create()
            ->setId($this->getId())
            ->setName($this->getName())
            ->setCreateAt($this->getCreateAt())
            ->setModifiedAt($this->getModifiedAt());
    }
}
