<?php

namespace Training\Repository\Model\ResourceModel\Example;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Training\Repository\Model\{
    Example as ExampleModel, 
    ResourceModel\Example as ExampleResourceModel
};


class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(ExampleModel::class, ExampleResourceModel::class);
        $this->_map['fields']['example_id'] = 'main_table.example_id';
        $this->_map['fields']['store'] = 'store_table.store_id';
    }
}
