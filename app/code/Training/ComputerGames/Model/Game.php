<?php

namespace Training\ComputerGames\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Api\AttributeValueFactory;
use Training\ComputerGames\Api\GameInterface;

class Game extends AbstractModel
    implements GameInterface
{
    protected $gameId;
    
    /**
     * @var AttributeValueFactory
     */
    protected $attributeValueFactory;
        
    
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        AttributeValueFactory $attributeValueFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        $this->attributeValueFactory = $attributeValueFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    
    protected function _construct()
    {
        $this->_init(ResourceModel\Game::class);
    }
    
    
    public function getId()
    {
        if (!$this->gameId) {
            $this->gameId = $this->getData(GameInterface::GAME_ID);
        }
        
        return $this->gameId;
    }

    public function setId($gameId)
    {
        $this->gameId = $gameId;
        $this->setData(GameInterface::GAME_ID, $gameId);
        return $this;
    }
        
    
    public function getCustomAttribute($attributeCode)
    {
        return $this
            ->attributeValueFactory
            ->create()
            ->setAttributeCode($attributeCode)
            ->setValue($this->getData($attributeCode));        
    }

    
    public function setCustomAttribute($attributeCode, $attributeValue)
    {
        $this->setData($attributeCode, $attributeValue);
        return $this;        
    }
        
    
    public function getCustomAttributes()
    {
        $output = [];
        foreach ($this->getData() as $key => $value) {
            $attribute = $this->attributeValueFactory->create();
            $output[] = $attribute->setAttributeCode($key)->setValue($value);
        }
        return $output;
    }


    public function setCustomAttributes(array $attributes)
    {
        foreach ($attributes as $attribute) {
            $this->setData(
                $attribute->getAttributeCode(),
                $attribute->getValue()
            );
        }
        return $this;
    }

    
    
    public function getCustomAttributesCodes()
    {
        return ['id', 'name', 'type', 'trial_period', 'release_date'];
    }
}
