<?php

namespace Training\ComputerGames\Model;

use Exception;
use Training\ComputerGames\Api\{GameRepositoryInterface, GameInterface, GameInterfaceFactory};
use Magento\Framework\Exception\{NoSuchEntityException, CouldNotSaveException, CouldNotDeleteException};

class GameRepository implements GameRepositoryInterface
{
    /**
     * @var ResourceModel\Game
     */
    protected $resourceModel;
    
    /**
     * @var GameInterfaceFactory
     */
    protected $gameFactory;
    
    public function __construct(
        ResourceModel\Game $resourceModel,
        GameInterfaceFactory $gameFactory
    ){
        $this->resourceModel = $resourceModel;
        $this->gameFactory = $gameFactory;
    }
    
    
    /**
     * @param int $gameId
     * @return type
     * @throws NoSuchEntityException
     */
    public function getById($gameId)
    {
        $game = $this->gameFactory->create();
        $game->load($gameId);
        if (!$game->getId()) {
            throw new NoSuchEntityException(__('Game with id "%1" does not exists', $gameId));
        }
        return $game;
    }
    

    public function save(GameInterface $game)
    {        
        try {
            $this->resourceModel->save($game);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Could not save game: %1', $e->getMessage()), $e);
        }
        return $game;
    }
    
    
    public function delete(GameInterface $game)
    {
        try {
            $this->resourceModel->delete($game);
        } catch(Exception $e) {
            throw new CouldNotDeleteException(__('Could not delete the game %1', $e->getMessage()));
        }
        return true;
    }
    
    public function deleteById($gameId)
    {
        return $this->delete($this->getById($gameId));
    }

}
