<?php

namespace Training\ComputerGames\Model\ResourceModel\Game;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Training\ComputerGames\Model\{
    Game as GameModel,
    ResourceModel\Game as GameResourceModel
};

class Collection extends AbstractCollection 
{
    protected function _construct()
    {
        $this->_init(GameModel::class, GameResourceModel::class);
    }
}