<?php

namespace Training\ComputerGames\Model\ResourceModel\Game\Grid;

use Magento\Framework\Api\{SearchCriteriaInterface, Search\SearchResultInterface};
use Training\ComputerGames\Model\ResourceModel\Game\Collection as BaseCollection;

class Collection extends BaseCollection
    implements SearchResultInterface
{   
    public function getAggregations()
    {
        return $this->aggregations;
    }

   
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }
    
    
    public function getSearchCriteria()
    {
        return null;
    }

    
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }
    

    public function getTotalCount()
    {
        return $this->getSize();
    }


    public function setTotalCount($totalCount)
    {
        return $this;
    }

    
    public function setItems(array $items = null)
    {
        return $this;
    }    
    
}
