<?php

namespace Training\ComputerGames\Setup;

use Magento\Framework\Setup\{InstallSchemaInterface, ModuleContextInterface, SchemaSetupInterface};
use Magento\Framework\DB\Ddl\Table as DdlTable;
      
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {        
        $setup->startSetup();
        $connection = $setup->getConnection();        
        
        $table = $connection
            ->newTable($setup->getTable('computer_game'))        
            ->addColumn('game_id', DdlTable::TYPE_SMALLINT, null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Block ID'
            )
            ->addColumn('name', DdlTable::TYPE_TEXT, 255, ['nullable' => false], 'Name')
            ->addColumn('type', DdlTable::TYPE_TEXT, 255, ['nullable' => false], 'Type')
            ->addColumn('trial_period', DdlTable::TYPE_SMALLINT, null, [], 'Trial Period')
            ->addColumn('release_date', DdlTable::TYPE_DATE, null, ['nullable' => true], 'Release Date')
            ->setComment('Computer games table');
        
        $connection->createTable($table);
        
        $setup->endSetup();
    }
}
