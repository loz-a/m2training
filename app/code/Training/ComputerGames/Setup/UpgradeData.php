<?php

namespace Training\ComputerGames\Setup;

use Magento\Framework\Setup\{UpgradeDataInterface, ModuleContextInterface, ModuleDataSetupInterface};
use Magento\Authorization\Model\UserContextInterface;
use Magento\Authorization\Model\{
    Acl\Role\Group as RoleGroup,
    RoleFactory,
    RulesFactory
};
use Magento\User\Model\{
    UserFactory, 
    ResourceModel\User as UserResource
};

class UpgradeData implements UpgradeDataInterface
{

    protected $roleFactory;
 
    protected $rulesFactory;
    
    protected $userFactory;
    
    protected $userResource;

    public function __construct(
        RoleFactory $roleFactory,
        RulesFactory $rulesFactory,
        UserFactory $userFactory,
        UserResource $userResource
    ){
        $this->roleFactory = $roleFactory;
        $this->rulesFactory = $rulesFactory;
        $this->userFactory = $userFactory;
        $this->userResource = $userResource;
    }
    
    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.1.1', '<')) {
            $this->upgrageVersionOneOneZero();
        }        
    }
    
    
    protected function upgrageVersionOneOneZero()
    {
        $role=$this->roleFactory->create();
        $role
            ->setName('Gamer') 
            ->setPid(0) 
            ->setRoleType(RoleGroup::ROLE_TYPE) 
            ->setUserType(UserContextInterface::USER_TYPE_ADMIN);
        $role->save();
        
        $resource = [
            'Magento_Backend::admin',
            'Training_ComputerGames::hello_page'
        ];
        
        $this
            ->rulesFactory
            ->create()
            ->setRoleId($role->getId())->setResources($resource)
            ->saveRel();
        
        
        $user = $this->userFactory->create();
//        $user->setRoleId($role->getId());
        
        $user->setData([
            'username' => 'cool_gamer',
            'firstname' => 'cg_firstname',
            'lastname' => 'cg_lastname',
            'email' => 'cg@mail.com',
            'password' => 'hello@123',
            'is_active' => 1,
            'interface_locale' => ' en_US',
            'role_id' => $role->getId()
        ]);
        
        $this->userResource->save($user);
    }

}
