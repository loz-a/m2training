<?php

namespace Training\ComputerGames\Api;


interface GameRepositoryInterface
{    
    public function save(GameInterface $game);

    public function getById($gameId);

//    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    public function delete(GameInterface $game);

    public function deleteById($gameId);
}
