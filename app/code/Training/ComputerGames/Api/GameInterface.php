<?php

namespace Training\ComputerGames\Api;

use Magento\Framework\Api\CustomAttributesDataInterface;

interface GameInterface extends CustomAttributesDataInterface
{
    const GAME_ID = 'game_id';
    
    public function getId();
    
    public function setId($gameId);
}
