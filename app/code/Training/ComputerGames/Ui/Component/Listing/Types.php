<?php

namespace Training\ComputerGames\Ui\Component\Listing;

use Magento\Framework\Data\OptionSourceInterface;

class Types implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return $this->options = [
            [
                'label' => 'RPG',
                'value' => 'RPG'
            ],
            [
                'label' => 'RTS',
                'value' => 'RTS'
            ],
            [
                'label' => 'MMO',
                'value' => 'MMO'
            ],
            [
                'label' => 'Simulator',
                'value' => 'Simulator'
            ],
            [
                'label' => 'Shooter',
                'value' => 'Shooter'
            ]
        ];
    }

}
