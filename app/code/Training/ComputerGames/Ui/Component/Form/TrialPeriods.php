<?php

namespace Training\ComputerGames\Ui\Component\Form;

use Magento\Framework\Data\OptionSourceInterface;

class TrialPeriods implements OptionSourceInterface
{
    public function toOptionArray()
    {
        return $this->options = [
            [
                'label' => 'No',
                'value' => '0'
            ],
            [
                'label' => 'Month',
                'value' => '1'
            ],
            [
                'label' => '2 Months',
                'value' => '2'
            ],
            [
                'label' => '6 Months',
                'value' => '6'
            ],
            [
                'label' => 'Year',
                'value' => '12'
            ]
        ];
    }
}
