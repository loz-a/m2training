<?php

namespace Training\ComputerGames\Ui\Component\Form;

use Magento\Framework\View\Element\UiComponent\DataProvider\FilterPool;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Training\ComputerGames\Model\ResourceModel\Game\CollectionFactory as GamesCollectionFactory;

class DataProvider extends AbstractDataProvider
{
    protected $collection;
    
    protected $eavConfig;
    
    protected $filterPool;
    
    protected $loadedData;
    
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        GamesCollectionFactory $collectionFactory,
        FilterPool $filterPool,
        array $meta = [],
        array $data = []
    ){
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->filterPool = $filterPool;
    }
    
    
    public function getData()
    {
        if (!$this->loadedData) {
            $items = $this->collection->getItems();
            $result = [];
            
            foreach ($items as $item) {
                $result['computer_games'] = $item->getData();
                $this->loadedData[$item->getGameId()] = $result;
                break;
            }
        }
        return $this->loadedData;
    }
}
