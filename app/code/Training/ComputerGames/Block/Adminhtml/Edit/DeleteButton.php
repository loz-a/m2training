<?php

namespace Training\ComputerGames\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class DeleteButton extends GenericButton
    implements ButtonProviderInterface
{
    public function getButtonData()
    {
        return [
            'label' => __('Delete Game'),
            'class' => 'delete',
            'id' => 'game-edit-delete-button',
//            'data_attribute' => [
//                'url' => $this->getDeleteUrl()
//            ],
//            'on_click' => '',
            'on_click' => 'deleteConfirm(\'' . __('Are you sure you want to do this?') . '\', \'' . $this->getDeleteUrl() . '\')',
            'sort_order' => 20
        ];
    }
    
    
    public function getDeleteUrl()
    {
        return $this->getUrl('*/index/delete', ['game_id' => $this->getGameId()]);
    }
}
