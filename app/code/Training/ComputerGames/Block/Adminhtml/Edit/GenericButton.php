<?php

namespace Training\ComputerGames\Block\Adminhtml\Edit;

use Magento\Framework\Registry;
use Magento\Backend\Block\Widget\Context;

class GenericButton
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    
    /**
     * @var Registry
     */
    protected $registry;
    
    
    public function __construct(Context $context, Registry $registry)
    {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->registry = $registry;
    }
    
    
    public function getGameId()
    {
        return $this->registry->registry('game_id');
    }
    
    
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
