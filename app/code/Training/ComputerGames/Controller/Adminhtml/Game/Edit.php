<?php

namespace Training\ComputerGames\Controller\Adminhtml\Game;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory as ResultPageFactory;
use Magento\Framework\Registry;

class Edit extends Action
{
    const ADMIN_RESOURCE = 'Training_ComputerGames::game_save';
    
    /**
     * @var Registry
     */
    protected $coreRegistry = null;
    
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    
    public function __construct(
        Action\Context $context,
        ResultPageFactory $resultPageFactory,
        Registry $registry
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        
        parent::__construct($context);
    }
    
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $id = $this->getRequest()->getParam('game_id');
        
        $this->coreRegistry->register('game_id', $id);
        return $resultPage;
    }

}
