<?php

namespace Training\ComputerGames\Controller\Adminhtml\Grid;

use Magento\Backend\App\{Action, Action\Context};
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Training_ComputerGames::game';
    
    protected $resultPageFactory;
    
    public function __construct(Context $context, PageFactory $pageFactory)
    {
        $this->resultPageFactory = $pageFactory;
        parent::__construct($context);
    }
    
    
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->getConfig()->getTitle()->prepend(__('Games'));

        return $resultPage;
    }
    
    
    protected function initPage($resultPage)
    {
        $resultPage
            ->setActiveMenu('Training_ComputerGames::games_list')
            ->addBreadcrumb(__('Games'), __('Games'));
        
        return $resultPage;
    }
    
    
//    protected function _isAllowed()
//    {
//        return $this->_authorization->isAllowed('Magento_Cms::page');
//    }

}
