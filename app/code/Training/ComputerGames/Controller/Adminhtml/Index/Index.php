<?php

namespace Training\ComputerGames\Controller\Adminhtml\Index;

use Magento\Backend\App\{Action, Action\Context};
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory;
    
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        
        parent::__construct($context);
    }

    
    public function execute()
    {
        return $this->resultPageFactory->create();
    }
    
    
    protected function isAllowed()
    {
        return $this->_authorization->isAllowed('Training_ComputerGames::hello_page');
    }

}
