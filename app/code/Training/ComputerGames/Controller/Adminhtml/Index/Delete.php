<?php

namespace Training\ComputerGames\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Training\ComputerGames\Api\GameRepositoryInterface;

class Delete extends Action
{
    const ADMIN_RESOURCE = 'Training_ComputerGames::game_delete';
    
     /**
     * @var GameRepositoryInterface
     */
    protected $gameRepo;
    
    public function __construct(
        Action\Context $context,        
        GameRepositoryInterface $gameRepo
    ){        
        $this->gameRepo = $gameRepo;
        
        parent::__construct($context);
    }
    
    
    public function execute()
    {
        $id = $this->getRequest()->getParam('game_id');        
        
        if (!$id) {
            $this->messageManager->addError(__('The game does not exists'));
        }
        
        try {
            $this->gameRepo->deleteById($id);
            $this->messageManager->addSuccess(__('The game has been deleted'));
        } catch (Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
        
        return $this->resultRedirectFactory->create()->setPath('*/grid/');
    }
}
