<?php

namespace Training\ComputerGames\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Training\ComputerGames\Api\GameRepositoryInterface;

class Save extends Action
{
    const ADMIN_RESOURCE = 'Training_ComputerGames::game_save';
    
    /**
     * @var GameFactory
     */
    protected $gameFactory;
    
    /**
     * @var GameRepositoryInterface
     */
    protected $gameRepo;
    
    public function __construct(
        Action\Context $context,        
        GameRepositoryInterface $gameRepo
    ){        
        $this->gameRepo = $gameRepo;
        
        parent::__construct($context);
    }

    
    
    public function execute()
    {
        $postValue = $this->getRequest()->getPostValue();
        $data = $postValue['computer_games'] ?? [];
        $resultRedirect = $this->resultRedirectFactory->create();
        
        $id = $data['game_id'] ?? null;
        
        if (!$id) {        
            $this->messageManager->addErrorMessage(__('This game no longer exists'));
            return $resultRedirect->setPath('*/grid/');            
        }
        
        try {
            $model = $this->gameRepo->getById($id);
        
            //@TODO do some validation here
        
            $model->setData($data);
        
            $this->gameRepo->save($model);
            $this->messageManager->addSuccessMessage(__('You saved the game.'));
            
        } catch(NoSuchEntityException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());   
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the page.'));   
        }
        
        return $resultRedirect->setPath('*/grid/');        
    }
}
