<?php

namespace Training\ComputerGames\Controller\Adminhtml\Index;

/**
 * Catalog category validate
 */
class Validate extends \Magento\Backend\App\Action
{    
    const ADMIN_RESOURCE = 'Training_ComputerGames::game_save';
    
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * AJAX category validation action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        //@TODO Do some validate here...
        $response = new \Magento\Framework\DataObject();
        $response->setError(0);

        $resultJson = $this->resultJsonFactory->create();
        $resultJson->setData($response);
        
        return $resultJson;
    }
}


