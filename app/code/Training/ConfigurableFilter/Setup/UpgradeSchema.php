<?php

namespace Training\ConfigurableFilter\Setup;

use Magento\Framework\Setup\{UpgradeSchemaInterface, ModuleContextInterface, SchemaSetupInterface};
use Magento\Framework\DB\Ddl\Table as DdlTable;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $dbVersion = $context->getVersion();
        
        if (version_compare($dbVersion, '1.0.2', '<')) {
            $setup->startSetup();
            $connection = $setup->getConnection();
            
            $connection->addColumn(
                'sales_order',
                'require_verification', 
                [
                    'type' => DdlTable::TYPE_SMALLINT,
                    'nullable' => true,
                    'default' => 1,
                    'comment' => 'Require Verification Flag'
                ]
            );
            
            $connection->addColumn(
                'sales_order_grid', 
                'require_verification', 
                [
                    'type' => DdlTable::TYPE_SMALLINT,
                    'nullable' => true,
                    'default' => 1,
                    'comment' => 'Require Verification Flag'                    
                ]
            );
            
            
            $setup->endSetup();
        }
    }

}
