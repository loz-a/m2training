<?php

namespace Training\ConfigurableFilter\Setup;

use Magento\Framework\Setup\{ModuleContextInterface, ModuleDataSetupInterface, UpgradeDataInterface};
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $eavSetup;
    
    public function __construct(EavSetup $eavSetup)
    {
        /**
         * @var ScopedAttributeInterface
         */
        $this->eavSetup = $eavSetup;
    }
    
    public function upgrade(
         ModuleDataSetupInterface $setup, 
         ModuleContextInterface $context
     ){
        $dbVersion = $context->getVersion();
        
        if (version_compare($dbVersion, '1.0.1', '<')) {
            $eav = $this->eavSetup;
            $entityTypeId = $eav->getEntityTypeId('catalog_product');
            $attributeSetId = $eav->getAttributeSetId($entityTypeId, 'bag');
            $attributeGroupId = $eav->getAttributeGroupId($entityTypeId, $attributeSetId, 'Product Details');
            $attributeCode = 'product_series';
            
            $properties = [
                'type' => 'varchar',
                'label' => 'Product Series',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'user_defined' => 1,
                'required' => 0,
                'visible_on_front' => 1,
                'is_used_in_grid' => 1,
                'is_visible_in_grid' => 1,
                'is_filterable_in_grid' => 1
            ];
            
            $eav->addAttribute($entityTypeId, $attributeCode, $properties);
            $eav->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, $attributeCode);
        }
    } 
}
