<?php

namespace Training\ConfigurableFilter\Observer;

use Magento\Framework\Event\{ObserverInterface, Observer};

class DisableRequireVerificationObserver implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $order->setRequireVerification(0);
        return $this;
    }

}
