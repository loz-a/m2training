<?php

namespace Training\ConfigurableFilter\Controller\Adminhtml\Order;

use Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter as MassActionFilter;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Verify extends AbstractMassAction
{
    public function __construct(
        Context $context, 
        MassActionFilter $filter,
        CollectionFactory $collectionFactory
    ){
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $filter);
    }

    
    protected function massAction(AbstractCollection $collection)
    {
        foreach ($collection as $order) {
            $order->setRequireVerification(0)->save();
        }        
        
        $redirect = $this->resultRedirectFactory->create();
        $redirect->setPath($this->getComponentRefererUrl());
        return $redirect;
    }

}
