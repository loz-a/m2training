<?php

namespace Training\ConfigurableFilter\Ui\DataProvider\Product;

use Magento\Ui\DataProvider\AddFilterToCollectionInterface;
use Magento\Framework\Data\Collection;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable\Attribute\Collection as ConfigurableProductCollection;
use Zend_Db_Select as DbSelect;

class AddConfigurableOptionsToCollection implements AddFilterToCollectionInterface
{
    /**
     * @var ConfigurableProductCollection
     */
    protected $configurableOptions = null;
    
    public function __construct(ConfigurableProductCollection $collection)
    {
        $this->configurableOptions = $collection;
    }
    
    public function addFilter(Collection $collection, $field, $condition = null)
    {
        $numberOfOptions = $condition['eq'] ?? null;
        
        if ($numberOfOptions){
            $select = $this
                ->configurableOptions
                ->getSelect()
                ->reset(DbSelect::COLUMNS)
                ->columns(['product_id', 'COUNT(*) as cnt'])
                ->group('product_id');
            
            $res = $this->configurableOptions->getConnection()->fetchAll($select);
            
            $ids = [];
            foreach ($res as $opt) {
                if ($opt['cnt'] === $numberOfOptions) {
                    $ids[] = $opt['product_id'];
                }
            }
            
            $collection->addFieldToFilter('entity_id', ['in'  => $ids]);
        }
    }
}
