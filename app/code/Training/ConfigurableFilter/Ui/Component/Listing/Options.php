<?php

namespace Training\ConfigurableFilter\Ui\Component\Listing;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable\Attribute\Collection as ConfigurableProductCollection;
use Zend_Db_Select as DbSelect;

class Options implements OptionSourceInterface
{
    /**
     * @var ConfigurableProductCollection
     */
    protected $configurableOptions = null;
    
    public function __construct(ConfigurableProductCollection $collection)
    {
        $this->configurableOptions = $collection;
    }
    
    public function toOptionArray()
    {
        $select = $this
            ->configurableOptions
            ->getSelect()
            ->reset(DbSelect::COLUMNS)
            ->distinct()
            ->columns(['COUNT(*) as cnt'])
            ->group('product_id');
        
        $numberOfOptions = $this->configurableOptions->getConnection()->fetchCol($select);
        
        $this->options = [];
        
        if (count($numberOfOptions)) {
            foreach ($numberOfOptions as $num) {
                $this->options[] = [
                    'label' => sprintf('%s option(s)', $num), 
                    'value' => $num
                ];
            }
        }        
        
        return $this->options;
    }

}
