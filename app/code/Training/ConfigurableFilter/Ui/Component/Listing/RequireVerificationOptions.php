<?php

namespace Training\ConfigurableFilter\Ui\Component\Listing;

use Magento\Framework\Data\OptionSourceInterface;

class RequireVerificationOptions implements OptionSourceInterface
{
    public function toOptionArray(): array
    {
        return $this->options = [
            ['label' => '0', 'value' => 0],
            ['label' => '1', 'value' => 1],
        ];
    }

}
