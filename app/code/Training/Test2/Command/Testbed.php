<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Training\Test2\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Testbed extends \Training\Test\Command\AbstractCommand
{
    protected function configure()
    {
        $this->setName('mf:test-di-for-Test2Module');
        $this->setDescription('A cli playground for testing commands');
        parent::configure();
    }
    
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->getObjectManager();
        $manager->get(\Training\Test\MagentoU\Test::class);        
        $output->writeln("Done!");
    }
}