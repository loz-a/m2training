<?php

namespace Training\Vendor\Setup;

use Magento\Framework\Setup\{UpgradeSchemaInterface, ModuleContextInterface, SchemaSetupInterface};
use Magento\Framework\DB\Ddl\Table as DdlTable;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            $setup->startSetup();
            
            $setup->getConnection()->addColumn(
                $setup->getTable('training_vendor_entity'),
                'comment',
                [
                    'type' => DdlTable::TYPE_TEXT,
                    'nullable' => true,
                    'lenght' => '1k',
                    'comment' => 'Comment Field'
                ]
            );
            
            $setup->endSetup();
        }
    }
}
