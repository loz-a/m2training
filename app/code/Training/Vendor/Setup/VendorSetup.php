<?php

namespace Training\Vendor\Setup;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Setup\CategorySetup;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Setup\Context;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class VendorSetup extends CategorySetup
{
    /**
     * Category setup factory
     *
     * @var CategorySetupFactory
     */
    protected $_setupFactory;

    /**
     * Init
     *
     * @param ModuleDataSetupInterface $setup
     * @param Context $context
     * @param CacheInterface $cache
     * @param CollectionFactory $attrGroupCollectionFactory     
     * @param CategorySetupFactory $setupFactory
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(
        ModuleDataSetupInterface $setup,
        Context $context,
        CacheInterface $cache,
        CollectionFactory $attrGroupCollectionFactory,        
        CategorySetupFactory $setupFactory,
        CategoryFactory $categoryFactory
    ) {
        $this->_setupFactory = $setupFactory;
        parent::__construct($setup, $context, $cache, $attrGroupCollectionFactory, $categoryFactory);
    }

    /**
     * Gets catalog setup
     *
     * @param array $data
     * @return CategorySetup
     */
    public function getCatalogSetup(array $data = [])
    {
        return $this->_setupFactory->create($data);
    }
}