<?php

namespace Training\Vendor\Setup;

use Magento\Framework\Setup\{InstallSchemaInterface, ModuleContextInterface, SchemaSetupInterface};
use Magento\Framework\DB\Ddl\Table as DdlTable;

class InstallSchema implements InstallSchemaInterface
{
    public function install(
        SchemaSetupInterface $setup, 
        ModuleContextInterface $context
    ){
        $setup->startSetup();
        
        $table = $setup->getConnection()
            ->newTable($setup->getTable('training_vendor_entity'))
            ->addColumn('vendor_id', DdlTable::TYPE_SMALLINT, null, 
                ['identity' => true, 'nullable' => false, 'primary' => true], 'Block ID'
            )
            ->addColumn('name', DdlTable::TYPE_TEXT, 255, ['nullable' => false], 'Vendor Name')
            ->addColumn('email', DdlTable::TYPE_TEXT, 255, ['nullable' => false], 'Vendor Email')
            ->addColumn('creation_time', DdlTable::TYPE_TIMESTAMP, null, [], 'Vendor Creation Time')
            ->addColumn('update_time', DdlTable::TYPE_TIMESTAMP, null, [], 'Vendor Modification Time')
            ->addColumn('is_active', DdlTable::TYPE_SMALLINT, null, ['nullable' => false, 'default' => 1], 'Is Vendor Active')
            ->setComment('Vendr Table');
        
        $setup->getConnection()->createTable($table);
        
        $setup->endSetup();
    }
}
