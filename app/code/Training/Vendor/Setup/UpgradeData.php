<?php

namespace Training\Vendor\Setup;

use Magento\Framework\Setup\{UpgradeDataInterface, ModuleContextInterface, ModuleDataSetupInterface};

class UpgradeData implements UpgradeDataInterface
{    
    private $vendorSetupFactory;

    public function __construct(VendorSetupFactory $vendorSetupFactory)
    {
        $this->vendorSetupFactory = $vendorSetupFactory;
    }
    
    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.3') < 0) {
            
            $vendorSetup = $this->vendorSetupFactory->create(['setup' => $setup]);
            $catalogInstaller = $vendorSetup->getCatalogSetup(['resourceName' => 'catalog_setup']);
            $catalogInstaller->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'some_attr',
                [
                    'name'      => 'Some New Attribute',
                    'group'     => 'Product Details',
                    'required'  => false,
                    //'label'     => 'Some Attribute'
                ]
            );
            
        }
        
        if (version_compare($context->getVersion(), '0.0.4') < 0) {
            $vendorSetup = $this->vendorSetupFactory->create(['setup' => $setup]);
            $catalogInstaller = $vendorSetup->getCatalogSetup(['resourceName' => 'catalog_setup']);
            $catalogInstaller->updateAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'some_attr',
                'frontend_label',
                'Some Attribute'
            );
        }
    }
}
