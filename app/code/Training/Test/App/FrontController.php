<?php

namespace Training\Test\App;

class FrontController extends \Magento\Framework\App\FrontController
{
    /**     
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    public function __construct(
        \Magento\Framework\App\RouterListInterface $routerList,
        \Magento\Framework\App\ResponseInterface $response,
        \Psr\Log\LoggerInterface $logger
    ) {        
        parent::__construct($routerList, $response);
        $this->logger = $logger;
    }
    
    
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        foreach ($this->_routerList as $router) {
            $this->logger->addDebug(get_class($router));
        }
        
        return parent::dispatch($request);
    }
}
