<?php

namespace Training\Test\Controller\Unit\Four;

use Magento\Framework\App\Action;
use Magento\Store\Model\ResourceModel\Store\CollectionFactory as StoreCollectionFactory;
use Magento\Framework\DataObject;
use Magento\Catalog\Model\ResourceModel\Category\{
    CollectionFactory as CategoryCollectionFactory,
    Collection as CategoryCollection
};

class Categories extends Action\Action
{
    /**     
     * @var \Magento\Store\Model\ResourceModel\Store\CollectionFactory
     */
    protected $storeCollectionFactory;
    
    /**     
     * @var Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $categoryCollectionFactory;

    public function __construct(
        Action\Context $context,
        StoreCollectionFactory $storeCollectionFactory,
        CategoryCollectionFactory $categoryCollectionFactory
    ){
        $this->storeCollectionFactory = $storeCollectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context);        
    }
    
    public function execute()
    {        
        $stores = $this->storeCollectionFactory->create()->getItems();
        
        $rootIds = [];        
        foreach ($stores as $store) {
            $rootIds[] = $store->getRootCategoryId();            
        }
          
        $categoryCollection = $this->categoryCollectionFactory->create();
        
        $categoryCollection
            ->addFieldToFilter('entity_id', ['in' => $rootIds])
            ->addNameToResult();
        
        $result = '';
        foreach ($categoryCollection as $categoryModel) {
            foreach ($stores as $store) {
                if ($categoryModel->getStoreId() == $store->getStoreId()) {                    
                    $result .= sprintf('Store: %s - Root Category Name: %s', $store->getName(), $categoryModel->getName());
                }
            }            
        }       
        
        $this->getResponse()->appendBody($result);
    }
}
