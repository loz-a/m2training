<?php

namespace Training\Test\Controller\Adminhtml\Hello;

class Index extends \Magento\Backend\App\Action
{

    public function execute()
    {        
        $url = $this->_url->getDirectUrl('catalog/category/view/id/4');
        $this->_redirect($url);
    }
    
    
    protected function _isAllowed()
    {        
        $secret = $this->getRequest()->getParam('secret', null);
        return (null !== $secret) && (int)$secret == 1;
    }
}
