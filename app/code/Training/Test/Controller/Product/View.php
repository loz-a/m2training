<?php

namespace Training\Test\Controller\Product;

class View
{    
    public function beforeExecute(\Magento\Catalog\Controller\Product\View $subject, ...$args) 
    {
        echo 'Hello in View Plugin in BEFORE execute method<BR>';
        return $args;
    }
    
    
    public function afterExecute(\Magento\Catalog\Controller\Product\View $subject, $result) 
    {
        echo 'Hello in View Plugin in AFTER execute method';
        return $result; 
    }
}
