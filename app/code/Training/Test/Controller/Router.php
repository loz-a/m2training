<?php

namespace Training\Test\Controller;

use Magento\Framework\App;

class Router implements App\RouterInterface
{
    /**     
     * @var \Magento\Framework\App\ActionFactory
     */
    protected $actionFactory;

    public function __construct(App\ActionFactory $actionFactory)
    {
        $this->actionFactory = $actionFactory;
    }
    
    
    public function match(App\RequestInterface $request)
    {
        $info = $request->getPathInfo();
        $matches = null;        
                
        if (preg_match("%^/(test)-(.*?)-(.*?)$%", $info, $matches)) {
            $request->setPathInfo(sprintf("/%s/%s/%s", $matches[1], $matches[2], $matches[3]));
            return $this->actionFactory->create(App\Action\Forward::class, ['request' => $request]);
        }
        return null;
    }
}
