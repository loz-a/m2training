<?php

namespace Training\Test\Controller\Action;

class Index extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        return $this->getResponse()->appendBody('Hello world');
    }
}
