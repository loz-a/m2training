<?php

namespace Training\Test\Controller\Block;

use Training\Test\Block\TemplateBlock;

class ViewTestBlock extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
        $block = $this->_view->getLayout()->createBlock(TemplateBlock::class);        
        $block->setTemplate('Training_Test::test.phtml');
        $this->getResponse()->appendBody($block->toHtml());
    }
}
