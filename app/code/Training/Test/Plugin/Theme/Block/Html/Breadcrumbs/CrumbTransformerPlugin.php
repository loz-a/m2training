<?php

namespace Training\Test\Plugin\Theme\Block\Html\Breadcrumbs;

class CrumbTransformerPlugin
{

    public function beforeAddCrumb(\Magento\Theme\Block\Html\Breadcrumbs $subject, $crumbName, $crumbInfo)
    {  
        $crumbLabel = $crumbInfo['label'];
        
        if (is_string($crumbLabel)) {
            $crumbInfo['label'] = sprintf('%s (!)', $crumbLabel);
        }
        else if ($crumbLabel instanceof \Magento\Framework\Phrase) {
            $labelText = sprintf('%s (!)', $crumbLabel->getText());
            $labelArgs = $crumbLabel->getArguments();
            $crumbInfo['label'] = new \Magento\Framework\Phrase($labelText, $labelArgs);            
        }
        
        return [$crumbName, $crumbInfo];
    }
}