<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Training\Test\Plugin\Theme\Block\Html\Footer;

/**
 * Description of CopyrightReplacerPlugin
 *
 * @author Андрій
 */
class CopyrightReplacerPlugin
{
    public function aroundGetCopyright()
    {
        return 'Customized copyright!';
    }
}
