<?php

namespace Training\Test\Plugin\Catalog\Block\Product\View;

use Magento\Catalog\Block\Product\View\Description as DescriptionBlock;

class DescriptionPlugin
{
    public function beforeToHtml(DescriptionBlock $subject, ...$args)
    {        
        //$subject->getProduct()->setDescription('Test description');
        $subject->setTemplate('Training_Test::description.phtml');
        return $args;
    }
}