<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Training\Test\Command;

use Symfony\Component\Console\Command\Command;
use \Magento\Framework\ObjectManagerInterface;

class AbstractCommand extends Command
{
    protected $objectManager;
    public function __construct(ObjectManagerInterface $manager)
    {
        $this->objectManager = $manager;
        parent::__construct();
    }
    
    protected function getObjectManager()
    {
        return $this->objectManager;
    }
    
}