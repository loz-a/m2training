<?php

namespace Training\Category\Plugin;

use Magento\Catalog\Api\Data\CategoryExtensionFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Category;

class AfterCategoryLoad
{   
    protected $categoryExtensionFactory;
        
    protected $setup;
    
    public function __construct(
        CategoryExtensionFactory $categoryExtensionFactory,
        ModuleDataSetupInterface $setup
    ) {
        $this->categoryExtensionFactory = $categoryExtensionFactory;
        $this->setup = $setup;
    }
    
    
    public function afterLoad(Category $category, $result) 
    {
        $categoryExtension = $category->getExtensionAttributes();
        
        if (null === $categoryExtension) {
            $categoryExtension = $this->categoryExtensionFactory->create();
        }
        
        $countries = array_column(
            $this->getCategoryCountries($category), 
            'country'
        );
        
        $categoryExtension->setCountries($countries);
        $category->setExtensionAttributes($categoryExtension);
        return $category;
    }
    
    
    protected function getCategoryCountries(Category $category)
    {
        $connection = $this->setup->getConnection();
        $select = $connection
            ->select()
            ->from('training_category_country')
            ->where('category_id=?', $category->getId());
        
        return $connection->fetchAll($select);
    }
}
