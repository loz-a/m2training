<?php

namespace Training\Category\Controller\Categories;

use Magento\Framework\App\Action\{Action, Context};
use Magento\Catalog\Api\CategoryRepositoryInterface;

class Index extends Action
{
    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepo;
    
    public function __construct(
        Context $context,
        CategoryRepositoryInterface $categoryRepo
    ){
        $this->categoryRepo = $categoryRepo;                
        
        parent::__construct($context);
    }

        
    public function execute()
    {
        $category = $this->categoryRepo->get(11);
        
        $extAttributes = $category->getExtensionAttributes();        
        
        $attributes = [
            'name' => $category->getName(),
            'countries' => $extAttributes->getCountries()
        ];            
        
        $this->outputResult($attributes);
    }

    
    protected function outputResult(array $attributes)
    {
        $outputs = [];
        foreach ($attributes as $name => $value) {
            $strValue = is_array($value) ? implode(', ', $value) : (string)$value;
            $outputs[] = sprintf('%s - %s', $name, $strValue);                
            
        }
        
        $result = sprintf('<html><body>%s</body></html>', implode('<br/>', $outputs));
        
        $this
            ->getResponse()
            ->setHeader('Content-Type', 'text/html')
            ->appendBody($result);
    }
}
