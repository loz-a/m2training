<?php

namespace Training\Render\Block;

use Magento\Framework\View\Element\AbstractBlock;

class SetPageTitleLinkBlock extends AbstractBlock
{
    protected function _toHtml()
    {        
        $href = $this->_urlBuilder->getUrl('render/onepage/setpagetitle');   
        $text = $this->getData('link_content') ?: 'Set Page Title';
        return sprintf('<a href="%s">%s</a>', $href, $text);
    }
}
