<?php

namespace Training\Render\Controller\Onepage;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\{Action, Context};

class SetPageTitle extends Action
{
    private $resultPageFactory;

    public function __construct(Context $context, PageFactory $pageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $pageFactory;
    }
    
    public function execute()
    {
        $page = $this->resultPageFactory->create();
        return $page;
    }
}