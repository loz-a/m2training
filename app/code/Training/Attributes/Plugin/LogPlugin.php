<?php

namespace Training\Attributes\Plugin;

use Psr\Log\LoggerInterface;
use Magento\Customer\Model\Customer\DataProvider;

class LogPlugin
{
    /**     
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
    
    public function afterGetData(DataProvider $subject, $result)
    {        
        $message = print_r($result, true);        
        $this->logger->info($message);
        return $result;
    }
}
