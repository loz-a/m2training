<?php

namespace Training\Attributes\Block;


class AlertBlock extends \Magento\Framework\View\Element\Template
{
    public function getFullActionName()
    {
        return $this->_request->getFullActionName();
    }
}
